# OME Data Model

[![pipeline status](https://gitlab.com/codelibre/ome/ome-model/badges/master/pipeline.svg)](https://gitlab.com/codelibre/ome/ome-model/commits/master)
[![Maven Central](https://img.shields.io/maven-central/v/net.codelibre/ome-xml.svg)](http://search.maven.org/#search%7Cgav%7C1%7Cg%3A%22net.codelibre%22%20AND%20a%3A%22ome-xml%22)
[![Javadocs](http://javadoc.io/badge/net.codelibre/ome-xml.svg)](http://javadoc.io/doc/net.codelibre/ome-xml)

OME data model specification, code generator and implementation.

This project was originally developed by the Open Microscopy
Environment in the [ome-model GitHub
repository](https://github.com/ome/ome-model).  The original
repository continues to be maintained, with a focus upon the Java
components.  This GitLab project is a fork, with its focus being C++
support which is no longer developed upstream, and is maintained by
Codelibre Consulting Limited.  It tracks upstream changes, and remains
API-compatible with upstream, and also provides additional features,
improvements and bugfixes not yet incorporated upstream on behalf of
its customers.  This currently includes the C++ implementation of the
OME-XML library, as well as support for Python 3 and Gradle.

Documentation
-------------

- [OME Data Model and File Formats](https://codelibre.gitlab.io/ome/ome-model/doc/)
- [C++ API reference](https://codelibre.gitlab.io/ome/ome-model/api/)
- [Java API reference](http://javadoc.io/doc/net.codelibre/ome-xml/)

Sample files
------------

- [OME-TIFF sample images](https://downloads.openmicroscopy.org/images/OME-TIFF/)
- [OME-XML sample images](https://downloads.openmicroscopy.org/images/OME-XML/)

Development
-----------

Codelibre Consulting Limited provides software development consulting
services for this and other projects.  Please [contact
us](mailto:consulting@codelibre.net) to discuss your requirements.

Alternatively, changes should be submitted
[upstream](https://github.com/ome/ome-model) in the first instance as
GitHub pull requests.  GitLab merge requests may be opened here when
upstream submission is not feasible.

Merge request testing
---------------------

We welcome merge requests from anyone.

Please verify the following before submitting a pull request:

 * verify that the branch merges cleanly into `master`
 * verify that the GitLab pipeline passes
 * verify that the branch only uses C++14 and Java 1.8 features (this should
   be tested by the pipeline)
 * make sure that your commits contain the correct authorship information and,
   if necessary, a signed-off-by line
 * make sure that the commit messages or pull request comment contains
   sufficient information for the reviewer to understand what problem was
   fixed and how to test it
