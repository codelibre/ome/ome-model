/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2015 - 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <filesystem>
#include <fstream>
#include <sstream>

#include "quantity.h"

using namespace ome::xml::model::enums;
using ome::xml::model::primitives::Quantity;

namespace
{

  test_map
  read_test_data()
  {
    const std::filesystem::path testdatafile(PROJECT_SOURCE_DIR "/ome-xml/src/test/cpp/data/units");

    std::ifstream is(testdatafile.string());
    is.imbue(std::locale::classic());

    test_map ret;

    std::string line;
    while (std::getline(is, line))
      {
        if (line.length() == 0) // Empty line; do nothing.
          {
          }
        else if (line[0] == '#') // Comment line
          {
          }
        else
          {
            std::istringstream lineinput(line);
            std::vector<std::string> tokens;
            std::string tmpinput;
            while(std::getline(lineinput, tmpinput, '\t'))
              {
                tokens.push_back(tmpinput);
              }

            if (tokens.size() == 9)
              {
                try
                  {
                    std::string from_unit = tokens[0];
                    std::string to_unit = tokens[1];
                    double to_value, from_value;
                    std::istringstream is2(tokens[2]);
                    is2 >> from_value;
                    if (!is2)
                      {
                        std::cerr << "Parse fail [f2]: " << tokens[2] << '\n';
                        throw std::runtime_error("Parse fail");
                      }
                    std::istringstream is3(tokens[3]);
                    is3 >> to_value;
                    if (!is3)
                      {
                        std::cerr << "Parse fail [f3]: " << tokens[3] << '\n';
                        throw std::runtime_error("Parse fail");
                      }
                    std::string to_output = tokens[4];
                    std::string from_symbol = tokens[5];
                    std::string to_symbol = tokens[6];
                    std::string model_output = tokens[7];
                    double precision;
                    std::istringstream is8(tokens[8]);
                    is8 >> precision;
                    if (!is8)
                      {
                        std::cerr << "Parse fail [f8]: " << tokens[8] << '\n';
                        throw std::runtime_error("Parse fail");
                      }

                    test_map::key_type k(from_symbol, to_symbol);
                    test_op v = { from_value, to_value, to_output, from_symbol, to_symbol, model_output, std::pow(10.0, precision) };

                    test_map::iterator i = ret.find(k);
                    if (i != ret.end())
                      {
                        i->second.push_back(v);
                      }
                    else
                      {
                        test_map::mapped_type vec;
                        vec.push_back(v);
                        ret.insert(test_map::value_type(k, vec));
                      }
                  }
                catch (const std::exception& e)
                  {
                    std::cerr << "Bad line " << line << ": " << e.what() << '\n';
                  }
              }
            else
              {
                std::cerr << "Bad line with "
                          << tokens.size() << " tokens: "
                          << line << '\n';
              }

          }
      }

    std::cerr << "Created " << ret.size() << " unique test sets\n";

    return ret;
  }

}

test_map test_data(read_test_data());
