/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#ifndef OME_XML_MODEL_PRIMITIVES_QUANTITY_H
#define OME_XML_MODEL_PRIMITIVES_QUANTITY_H

#include <ostream>

namespace ome
{
  namespace xml
  {
    namespace model
    {
      namespace primitives
      {

        /**
         * A quantity of a defined unit.
         */
        template<class Unit, typename Value = double>
        class Quantity
        {
        public:
          /// The type of a unit.
          typedef Unit unit_type;
          /// The type of a value.
          typedef Value value_type;

          /**
           * Default construct a Quantity.
           */
          inline
          Quantity ():
            value(),
            unit(typename unit_type::enum_value(0))
          {
          }

          /**
           * Construct a Quantity from a value and unit.
           *
           * @param value the quantity value.
           * @param unit the quantity unit.
           */
          inline
          Quantity (value_type value,
                    unit_type  unit):
            value(value),
            unit(unit)
          {
          }

          /**
           * Copy construct a Quantity.
           *
           * @param quantity the quantity to copy.
           */
          inline
          Quantity(const Quantity& quantity):
            value(quantity.value),
            unit(quantity.unit)
          {
          }

          /// Destructor.
          inline
          ~Quantity ()
          {
          }

          /**
           * Get the value for this quantity.
           *
           * @returns the value.
           */
          value_type
          getValue() const
          {
            return value;
          }

          /**
           * Get the unit for this quantity.
           *
           * @returns the unit.
           */
          unit_type
          getUnit() const
          {
            return unit;
          }

          /**
           * Assign the quantity from a quantity.
           *
           * @param quantity the quantity to assign.
           * @returns the new quantity.
           */
          inline Quantity&
          operator= (const Quantity& quantity)
          {
            this->value = quantity.value;
            this->unit = quantity.unit;
            return *this;
          }

        private:
          /// Quantity value.
          value_type value;
          /// Quantity unit.
          unit_type unit;
        };

        /**
         * Output Quantity to output stream.
         *
         * @param os the output stream.
         * @param quantity the Quantity to output.
         * @returns the output stream.
         */
        template<class charT, class traits, typename Unit, typename Value>
        inline std::basic_ostream<charT,traits>&
        operator<< (std::basic_ostream<charT,traits>& os,
                    const Quantity<Unit, Value>& quantity)
        {
          return os << quantity.getValue() << ' ' << quantity.getUnit();
        }

        /**
         * Set Quantity from input stream.
         *
         * @param is the input stream.
         * @param quantity the Quantity to set.
         * @returns the input stream.
         */
        template<class charT, class traits, typename Unit, typename Value>
        inline std::basic_istream<charT,traits>&
        operator>> (std::basic_istream<charT,traits>& is,
                    Quantity<Unit, Value>& quantity)
        {
          Value v;
          Unit u(typename Unit::enum_value(0));
          if (is >> v >> u)
            {
              quantity = Quantity<Unit, Value>(v, u);
            }
          return is;
        }

      }
    }
  }
}

#endif // OME_XML_MODEL_PRIMITIVES_QUANTITY_H

/*
 * Local Variables:
 * mode:C++
 * End:
 */
