/*
 * #%L
 * OME-XML C++ library for working with OME-XML metadata structures.
 * %%
 * Copyright © 2016 Open Microscopy Environment:
 *   - Massachusetts Institute of Technology
 *   - National Institutes of Health
 *   - University of Dundee
 *   - Board of Regents of the University of Wisconsin-Madison
 *   - Glencoe Software, Inc.
 * Copyright © 2018 Quantitative Imaging Systems, LLC
 * Copyright © 2020 Roger Leigh
 * %%
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice,
 *    this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright notice,
 *    this list of conditions and the following disclaimer in the documentation
 *    and/or other materials provided with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 *
 * The views and conclusions contained in the software and documentation are
 * those of the authors and should not be interpreted as representing official
 * policies, either expressed or implied, of any organization.
 * #L%
 */

#include <ome/common/module.h>
#include <ome/xml/module.h>

#include <ome/xml/OMETransformResolver.h>

#include <fmt/format.h>

#include <algorithm>

namespace ome
{
  namespace xml
  {

    OMETransformResolver::OMETransformResolver():
        transformdir()
    {
      // Hack to force module registration when static linking.
      register_module_paths();
      transformdir = ome::common::module_runtime_path("ome-xml-transform");
    }

    OMETransformResolver::OMETransformResolver(const std::filesystem::path& transformdir):
      transformdir(transformdir)
    {
    }

    OMETransformResolver::~OMETransformResolver()
    {
    }

    const std::set<std::string>&
    OMETransformResolver::schema_versions() const
    {
      static const std::set<std::string> allTransforms = {"2003-FC", "2007-06",
                                                          "2008-02", "2008-09",
                                                          "2009-09", "2010-04",
                                                          "2010-06", "2011-06",
                                                          "2012-06", "2013-06",
                                                          "2015-01", "2016-06"};
      return allTransforms;
    }

    std::vector<std::filesystem::path>
    OMETransformResolver::transform_order(const std::string& source,
                                          const std::string& target) const
    {
      // Standard upgrade sequence.
      static const std::vector<std::string> standardSequence = {"2003-FC", "2008-09",
                                                                "2009-09", "2010-04",
                                                                "2010-06", "2011-06",
                                                                "2012-06", "2013-06",
                                                                "2015-01", "2016-06"};

      // Alternate upgrade sequence for 2007-06.
      static const std::vector<std::string> alternateSequence2007 = {"2007-06", "2008-09",
                                                                     "2009-09", "2010-04",
                                                                     "2010-06", "2011-06",
                                                                     "2012-06", "2013-06",
                                                                     "2015-01", "2016-06"};

      // Alternate upgrade sequence for 2008-02.
      static const std::vector<std::string> alternateSequence2008 = {"2008-02", "2008-09",
                                                                     "2009-09", "2010-04",
                                                                     "2010-06", "2011-06",
                                                                     "2012-06", "2013-06",
                                                                     "2015-01", "2016-06"};

      const std::vector<std::string> *sequence = &standardSequence;
      if (source == "2007-06")
      {
        sequence = &alternateSequence2007;
      }
      else if (source == "2008-02")
      {
        sequence = &alternateSequence2008;
      }
      std::vector<std::filesystem::path> order;

      auto start = std::find(sequence->cbegin(), sequence->cend(), source);
      if (start == sequence->cend())
      {
        throw std::runtime_error("Impossible to determine transform order: source invalid or missing");
      }

      auto end = std::find(sequence->cbegin(), sequence->cend(), target);
      if (start == sequence->cend())
      {
        throw std::runtime_error("Impossible to determine transform order: target invalid or missing");
      }

      auto from = start;
      for (auto to = start + 1; to < end + 1; ++to)
      {
        if (to == sequence->cend() && *to != target)
        {
          throw std::runtime_error("Invalid transform sequence");
        }
        std::string filename = fmt::format("{0}-to-{1}.xsl",
                                           *from, *to);
        order.push_back(transformdir / filename);

        from = to;
      }

      return order;
    }

  }
}
